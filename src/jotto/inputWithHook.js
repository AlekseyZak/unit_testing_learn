import React from 'react';
import PropTypes from 'prop-types';

function InputWithHook({ secretWord }) {
    const [currentGuess, setCurrentGuess] = React.useState('');

    return (
        <div data-test="component-input">
            <form className="form-inline">
                <input
                    className="mb-2 mx-sm-3"
                    data-test="input-box"
                    type="text"
                    placeholder="enter guess"
                    value={currentGuess}
                    onChange={(event) => setCurrentGuess(event.target.value )}
                />
                <button
                    className="btn btn-success mb-2"
                    data-test="submit-button"
                    type="submit"
                    onClick={(event) => {
                        event.preventDefault();
                        setCurrentGuess('')
                    }}
                >
                    Submit
                </button>
            </form>
        </div>
    )
}

InputWithHook.propTypes = {
    secretWord: PropTypes.string.isRequired,
}

export default InputWithHook;