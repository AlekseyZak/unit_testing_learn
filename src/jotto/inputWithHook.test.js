import React, { useState } from 'react';
import { shallow } from 'enzyme';
import {checkProps, findByTestAttr, storeFactory} from '../../test/testUtils';
import InputWithHook from './inputWithHook';

/**
 * Factory function to create a ShallowWrapper for the GuessedWords component.
 * @function setup
 * @param {object} secretWord - Initial state for this setup.
 * @returns {ShallowWrapper}
 */

const setup = (secretWord='party') => {
    return shallow(<InputWithHook secretWord={secretWord} />);
};

test('renders component without error', () => {
    const wrapper = setup();
    const component = findByTestAttr(wrapper, 'component-input');
    expect(component.length).toBe(1)
});

test('does not throw warning with expected props', () => {
    checkProps(InputWithHook, { secretWord: "party" })
});

describe('state controlled input field', () => {
    let mockSetCurrentGuess = jest.fn();
    let wrapper;
    beforeEach(() => {
        mockSetCurrentGuess.mockClear();
        React.useState = jest.fn(() => ['', mockSetCurrentGuess]);
        wrapper = setup()
    })
    test('state updates with value of input box upon change', () => {
        const inputBox = findByTestAttr(wrapper, 'input-box');
        expect(inputBox.length).toBe(1)

        const mockEvent = { target: { value: 'train' } };
        inputBox.simulate("change", mockEvent);
        expect(mockSetCurrentGuess).toHaveBeenCalledWith('train');
    });

    test('field is cleared upon submit button click', () => {
        const buttonSubmit = findByTestAttr(wrapper, 'submit-button');
        expect(buttonSubmit.length).toBe(1)

        buttonSubmit.simulate("click", { preventDefault() {} });
        expect(mockSetCurrentGuess).toHaveBeenCalledWith('');
    });
})