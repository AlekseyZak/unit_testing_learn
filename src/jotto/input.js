import React, { Component } from 'react';
import { connect } from 'react-redux';
import { guessWord } from '../actions';

export class UnconnectedInput extends Component {
    /**
     * @method constructor
     * @param {object} props - Component props.
     * @returns {underfined}
     */

    constructor(props) {
        super(props);

        this.state = { currentGuess: null }

        this.submitGuessedWord = this.submitGuessedWord.bind(this)
    }

    submitGuessedWord(event) {
        event.preventDefault();
        const guessedWord = this.state.currentGuess;

        if(guessedWord && guessedWord.length > 0) {
            this.props.guessWord(guessedWord);
            this.setState({ currentGuess: '' })
        }
    }

    render() {
        const { guessWord } = this.props;
        const contents = this.props.success
        ? null
        : (
            <form className="form-inline">
                <input
                    className="mb-2 mx-sm-3"
                    data-test="input-box"
                    type="text"
                    placeholder="enter guess"
                    value={this.state.currentGuess}
                    onChange={(event) => this.setState({ currentGuess: event.target.value })}
                />
                <button
                    className="btn btn-success mb-2"
                    data-test="submit-button"
                    type="submit"
                    onClick={(event) => this.submitGuessedWord(event)}
                >
                    Submit
                </button>
            </form>
            )
        return (
            <div data-test="component-input">{contents}</div>
        )
    }
}

const mapStateToProps = ({ success }) => {
    return { success };
}

export default connect(mapStateToProps, { guessWord })(UnconnectedInput)