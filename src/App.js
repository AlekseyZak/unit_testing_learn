import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSecretWord } from './actions'

import Input from './jotto/inputWithHook';
import Congrats from './jotto/congrats';
import GuessedWords from './jotto/guessedWords';
import hookActions from './actions/hookActions';

/**
 * reducer to update state, called automatically by dispatch
 * @param state {object} - existing state
 * @param action {object} - contains 'type' and 'payload' properties for the state updated
 *      for example: { type: "setSecretWord", payload: "party" }
 * @returns {object} - new state
 */

function reducer(state, action) {
    switch(action.type) {
        case "setSecretWord":
            return { ...state, secretWord: action.payload }
        default:
            throw new Error(`Invalid action type ${action.type}`);
    }
}

function App() {
    const [state, dispatch] = React.useReducer(
        reducer,
        { secretWord: null },
    )

    const setSecretWord = (secretWord) =>
        dispatch({ type: "setSecretWord", payload: secretWord });

    React.useEffect(() => {
        hookActions.getSecretWord(setSecretWord)
    }, []);

    if (!state.secretWord) {
        return (
            <div className="container" data-test="component-spinner">
                <div className="spinner-border" role="status">
                    <span className="sr-only">Loading...</span>
                </div>
                <p>Loading secret word</p>
            </div>
        )
    };

    return (
            <div className="container" data-test="component-app">
                <h1>Jotto</h1>
                <Input secretWord={state.secretWord}/>
                {/*<Congrats success={success} />*/}
                {/*<GuessedWords guessedWords={guessedWords}/>*/}
            </div>
        );
}

// export class UnconnectedApp extends Component {
//
//
//     componentDidMount() {
//         // get the secret word
//         this.props.getSecretWord();
//     }
//
//     render() {
//         const { success, guessedWords, secretWord } = this.props;
//
//         return (
//             <div className="container" data-test="component-app">
//                 <h1>Jotto</h1>
//                 <Input />
//                 <Congrats success={success} />
//                 <GuessedWords guessedWords={guessedWords}/>
//             </div>
//         );
//     }
// }
//
// const mapStateToProps = ({ success, guessedWords, secretWord }) => {
//     return { success, guessedWords, secretWord };
// }
//
// export default connect(mapStateToProps, { getSecretWord })(UnconnectedApp);

export default App;
