import React from 'react';
import App from './App';
import { mount } from 'enzyme';

import { findByTestAttr, storeFactory } from '../test/testUtils';
import hookActions from './actions/hookActions'

const mockGetSecretWord = jest.fn();


/**
 * Setup function for app component.
 * @param {string} secretWord - desired secretWord state value for test
 * @returns {ReactWrapper}
 */

const setup = (secretWord="party") => {
  mockGetSecretWord.mockClear();
  hookActions.getSecretWord = mockGetSecretWord;

  const mockUseReducer = jest.fn()
      .mockReturnValue([
        { secretWord },
          jest.fn()
      ])

  React.useReducer = mockUseReducer;

  //use mount because useEffect not called on 'shallow'
  return mount(<App />);
}

test('renders without error', () => {
  const wrapper = setup();
  const appComponent = findByTestAttr(wrapper, "component-app");
  expect(appComponent.length).toBe(1);
});

describe('getSecretWord calls', () => {
  test('getSecretWord gets called on App mount', () => {
    setup();

    // check to see if secret word was updated
    expect(mockGetSecretWord).toHaveBeenCalled();
  });

  test('secretWord does not update on App update', () => {
    const wrapper = setup();
    mockGetSecretWord.mockClear();

    wrapper.setProps();

    expect(mockGetSecretWord).not.toHaveBeenCalled();
  });
})

describe('secretWord is not null', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = setup("party");
  });

  test('renders app when secretWord is not null', () => {
    const appComponent = findByTestAttr(wrapper, 'component-app');
    expect(appComponent.exists()).toBe(true)
  });

  test('does not render spinner when secretWord is not null', () => {
    const spinnerComponent = findByTestAttr(wrapper, 'component-spinner');
    expect(spinnerComponent.exists()).toBe(false)
  });
});

describe('secretWord is null', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = setup(null);
  });

  test('does not render app when secretWord is null', () => {
    const appComponent = findByTestAttr(wrapper, 'component-app');
    expect(appComponent.exists()).toBe(false)
  });

  test('renders spinner when secretWord is null', () => {
    const spinnerComponent = findByTestAttr(wrapper, 'component-spinner');
    expect(spinnerComponent.exists()).toBe(true)
  });
})

// describe('redux properties', () => {
//   test('has access to `success` state', () => {
//     const success = true;
//     const wrapper = setup({ success });
//     const successProp = wrapper.instance().props.success;
//     expect(successProp).toBe(success);
//   });
//
//   test('has access to `secretWord` state', () => {
//     const secretWord = 'party';
//     const wrapper = setup({ secretWord });
//     const secretWordProp = wrapper.instance().props.secretWord;
//     expect(secretWordProp).toBe(secretWord);
//   });
//
//   test('has access to `guessedWords` state', () => {
//     const guessedWords = [{guessedWord: 'train', letterMatchCount: 3}];
//     const wrapper = setup({ guessedWords });
//     const guessedWordsProp = wrapper.instance().props.guessedWords;
//     expect(guessedWordsProp).toEqual(guessedWords);
//   });
//
//   test('`getSecretWord` action creator is a function on the props', () => {
//     const wrapper = setup();
//     const getSecretWord = wrapper.instance().props.getSecretWord;
//     expect(getSecretWord).toBeInstanceOf(Function);
//   });
//
//   test('`getSecretWord` runs on App mount', () => {
//     const getSecretWordMock = jest.fn();
//
//     const props = {
//       getSecretWord: getSecretWordMock,
//       success: false,
//       guessedWords: [],
//     }
//
//     // set up app component with getSecretWord Mock as the getSecretWord prop
//     const wrapper = shallow(<UnconnectedApp {...props} />);
//
//     // run lifeCycle method
//     wrapper.instance().componentDidMount();
//
//     // check to see if mock ran
//     const getSecretWordCallCount = getSecretWordMock.mock.calls.length;
//     expect(getSecretWordCallCount).toBe(1);
//   })
// });
