import { actionTypes } from '../actions';
/**
 *
 * @param {boolean} state - Array of guessed words.
 * @param {object} action - Action to be reduced.
 * @returns {boolean|null} - New success state.
 */

export default (state= false, action) => {
    switch (action.type) {
        case (actionTypes.CORRECT_GUESS):
            return true;

        default:
            return state
    }
}